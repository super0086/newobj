package com.mervyn.newobj.trial;

import android.util.Log;

import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationHandler;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.lang.reflect.Proxy;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

/**
 * Description: 业务请求分发控制器
 * Created by Wangchao on 2018-04-12 16:50.
 */
public final class DispenseController {

    private final static String TAG = DispenseController.class.getSimpleName();

    private final Map<String, Method> methodCache = new ConcurrentHashMap<>();

    private final Map<String, Object> moduleInstanceCache = new ConcurrentHashMap<>();

    private DispenseController() {
    }

    private static class DispenseControllerHolder {
        private static final DispenseController INSTANCE = new DispenseController();
    }

    public static DispenseController getInstance() {
        return DispenseControllerHolder.INSTANCE;
    }

    /**
     * 业务分发
     *
     * @param moduleClass
     * @param service
     * @param <T>
     * @return
     */
    public <T> T dispense(final Class moduleClass, final Class<T> service) {
        if (moduleClass == null) {
            Log.e(TAG, "module class is null");
            return (T) new Object();
        }
        if (service == null) {
            Log.e(TAG, "service method is null");
            return (T) new Object();
        }
        return (T) Proxy.newProxyInstance(service.getClassLoader(), new Class<?>[]{service},
                new InvocationHandler() {

                    @Override
                    public Object invoke(Object proxy, Method method, Object[] objects) {
                        return businessMethodInvoke(moduleClass, method, objects);
                    }

                });
    }

    /**
     * 业务方法处理后调用
     *
     * @param moduleClass
     * @param method
     * @param objects
     * @return
     */
    private Object businessMethodInvoke(Class moduleClass, Method method, Object[] objects) {
        try {
            String moduleClassName = moduleClass.getName();
            Object business = moduleInstanceCache.get(moduleClassName);
            if (business == null) {
                Constructor<?> constructor = moduleClass.getConstructor();
                business = constructor.newInstance();
                moduleInstanceCache.put(moduleClassName, business);
                Log.i(TAG, "businessMethodInvoke moduleClassName [" + moduleClassName + "] is null new instance");
            } else {
                Log.i(TAG, "businessMethodInvoke moduleClassName [" + moduleClassName + "] instance from cache");
            }

            Class<?> methodClazz = business.getClass();
            String methodName = method.getName();
            Method methodBusiness = methodCache.get(moduleClassName + methodName);

            if (methodBusiness == null) {
                methodBusiness = methodClazz.getDeclaredMethod(methodName, Object[].class);
                methodCache.put(moduleClassName + methodName, methodBusiness);
                Log.i(TAG, "businessMethodInvoke methodBusiness [" + methodName + "] is null new instance");
            } else {
                Log.i(TAG, "businessMethodInvoke methodBusiness [" + methodName + "] instance from cache");
            }
            if (methodBusiness == null) {
                Log.e(TAG, "method is null exception");
                return null;
            }
            return methodBusiness.invoke(business, objects[0]);
        } catch (NoSuchMethodException e) {
            e.printStackTrace();
            Log.e(TAG, "module method exception:" + e.getMessage());
            return null;
        } catch (IllegalAccessException e) {
            e.printStackTrace();
            Log.e(TAG, "module method exception:" + e.getMessage());
            return null;
        } catch (InstantiationException e) {
            e.printStackTrace();
            Log.e(TAG, "module method exception:" + e.getMessage());
            return null;
        } catch (InvocationTargetException e) {
            e.printStackTrace();
            Log.e(TAG, "module method exception:" + e.getMessage());
            return null;
        }
    }

}